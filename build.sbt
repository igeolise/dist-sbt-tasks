sbtPlugin     := true
organization  := "com.igeolise.sbt"
name          := "sbt-dist-tasks"
version       := "1.1.0"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",  // yes, this is 2 args
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture"
)

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.3")

libraryDependencies ++= Seq(
  "com.igeolise"    %% "dist-info" % "1.2.0",
  "org.kamranzafar" %  "jtar"      % "2.3")
