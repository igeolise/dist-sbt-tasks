package com.igeolise.sbt

import java.io.{BufferedOutputStream, FileOutputStream, OutputStream}
import java.nio.file.attribute.PosixFilePermission
import java.util.zip.{GZIPOutputStream, ZipEntry, ZipOutputStream}

import org.kamranzafar.jtar.{TarEntry, TarOutputStream}
import sbt.Keys._
import sbt._

import scala.collection.JavaConverters._


/**
  * Provides legacy distArchive task used only in build script of igeolise-lib and traveltime-api (V3) projects.
  * Can not be used together with `DistInfoPlugin`.
  */
object DistArchivePlugin extends AutoPlugin
{
  object Modes
  {
    val m755 = Set(
      PosixFilePermission.OWNER_READ,
      PosixFilePermission.OWNER_WRITE,
      PosixFilePermission.OWNER_EXECUTE,
      PosixFilePermission.GROUP_READ,
      PosixFilePermission.GROUP_EXECUTE,
      PosixFilePermission.OTHERS_READ,
      PosixFilePermission.OTHERS_EXECUTE
    ).asJava
  }

  sealed trait FileFormat
  {
    val extension: String

    def stream(file: File): FileFormat.FormatOutputStream
  }

  object FileFormat
  {
    trait FormatOutputStream
    {
      protected val out: OutputStream

      def store(file: File, name: String): Unit

      def close(): Unit = out.close()
    }

    protected def bufferedOut(file: File): BufferedOutputStream =
      new BufferedOutputStream(new FileOutputStream(file))

    private[this] class TarOutput(file: File, compress: Boolean)
      extends FormatOutputStream
    {
      protected val out = {
        val out = bufferedOut(file)
        new TarOutputStream(if (compress) new GZIPOutputStream(out) else out)
      }

      def store(file: File, name: String) = {
        out.putNextEntry(new TarEntry(file, name))
        if (!file.isDirectory) out.write(IO.readBytes(file))
      }
    }

    // Output dist archive in tar format.
    case object Tar extends FileFormat
    {
      val extension = "tar"

      def stream(file: File): FormatOutputStream =
        new TarOutput(file, compress = false)
    }

    // Output dist archive in gziped tar format.
    case object TarGz extends FileFormat
    {
      val extension = "tar.gz"

      def stream(file: File): FormatOutputStream =
        new TarOutput(file, compress = true)
    }

    // Output dist archive in zip format.
    case object Zip extends FileFormat
    {
      val extension = "zip"

      def stream(file: File) = new FormatOutputStream
      {
        protected val out = new ZipOutputStream(bufferedOut(file))

        def store(file: File, name: String) = {
          if (!file.isDirectory) {
            out.putNextEntry(new ZipEntry(name))
            out.write(IO.readBytes(file))
          }
        }
      }
    }
  }

  val distInfo = TaskKey[File]("dist-info", "Gathers information about the distribution")
  val distInfoAdditionalProperties = settingKey[Map[String, String]](
    "Any additional properties that need to go into a distribution metadata file")
  val distArchive = TaskKey[File]("dist-archive", "Creates archive from given dist directory")

  def distInfoTask(dist: TaskKey[File]) =
    distInfo := {
      val distV             = dist.value
      val streamsV          = streams.value
      val additionalPropsV  = distInfoAdditionalProperties.value
      streamsV.log.info("Running dist-info task.")
      DistInfo.write(distV / "meta" / "dist_info", additionalPropsV)
      distV
    }

  /**
    * Given dist task key create a task which creates tgz archive from given
    * dist directory.
    *
    * @param dist    task for building a distribution
    * @param format  what file format shall we use
    */
  def distArchiveTask(dist: TaskKey[File], format: FileFormat) =
    distArchive := {
      val distV     = dist.value
      val targetV   = target.value
      val streamsV  = streams.value

      val files = PathFinder(distV).**(AllPassFilter).pair(Path.rebase(distV, ""))
      val archiveFile =
        targetV / "%s.%s".format(distV.getName, format.extension)

      streamsV.log.info("Building archive: " + archiveFile.getCanonicalPath)

      val archiveStream = format.stream(archiveFile)

      files.foreach { case (file, archiveName) =>
        streamsV.log.debug(
          "Storing %s as %s".format(file.getCanonicalPath, archiveName)
        )
        archiveStream.store(file, archiveName)
      }
      archiveStream.close()
      streamsV.log.info("Archive '%s' built.".format(archiveFile.getCanonicalPath))

      archiveFile
    }
}
