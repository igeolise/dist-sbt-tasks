package com.igeolise.sbt

import java.io.FileOutputStream

import com.typesafe.sbt.packager.universal.UniversalPlugin
import com.typesafe.sbt.packager.universal.UniversalPlugin.autoImport._
import org.joda.time.DateTime
import sbt.Keys.{mappings, resourceManaged}
import sbt._
import scala.sys.process._

import scalaz.\/


/**
  * Writes metadata about the distribution to a file.
  * Can not be used together with `DistArchivePlugin`.
  */
object DistInfoPlugin extends AutoPlugin
{
  override def requires = UniversalPlugin

  object autoImport
  {
    val distInfo = taskKey[File]("Gathers information about the distribution")
    val distInfoAdditionalProperties = settingKey[Map[String, String]](
      "Any additional properties that need to go into a distribution metadata file")

    lazy val baseDistInfoSettings: Seq[Setting[_]] = Seq(
      distInfoAdditionalProperties := Map.empty,
      distInfo := {
        val file = (resourceManaged in Compile).value / "dist_info"
        val additionalProperties = distInfoAdditionalProperties.value
        DistInfo.write(file, additionalProperties).valueOr { throw _ }
      }
    ) ++
      inConfig(Universal)(mappings += distInfo.value -> "meta/dist_info")
  }

  override def projectSettings = autoImport.baseDistInfoSettings
}

object DistInfo
{
  def gitCommit: String = runOr("git rev-parse HEAD", "Unknown commit")
  def gitBranch: String = runOr("git rev-parse --abbrev-ref HEAD", "Unknown branch")

  def write(file: File, additionalProperties: Map[String, String]): Throwable \/ File = {
    val commit  = gitCommit
    val branch  = gitBranch
    val builder = runOr("git config user.name", "Unknown builder")

    file.getParentFile.mkdirs()
    file.createNewFile()

    import com.igeolise.distinfo.{DistInfo => DistInfoValue}
    val distInfo = DistInfoValue(branch, commit, builder, new DateTime(), additionalProperties)
    DistInfoValue.write(distInfo, new FileOutputStream(file)).map { _ => file }
  }

  private def runOr(command: String, fallback: String): String =
    try {
      Process(command).lineStream.headOption.getOrElse(fallback)
    }
    catch {
      case _: Exception => fallback
    }
}